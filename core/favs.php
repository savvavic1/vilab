<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);?>
<?php
//neprihlaseny nema pristup
if (!isset($_SESSION['user'])) {
	echo "<a href='../mainpage.php'>Log in</a> to continue.";
}
else {?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>My Items</title>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="../styles/favsstyles.css" type="text/css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	</head>
	<body>
		<br>
		<h1><a href="browsing2.php">Back to Browsing</a></h1><br>
		
		<h1><a href="#">Back to Top</a></h1><br>

		<form>
			<button type="submit" class="home" formaction="../mainpage.php">Home</button>
		</form>
		
		<form>
			<button type="submit" class="logout" formaction="logout.php">Log Out</button>
		</form>

		<?php
		//zajisteni promenne s cislem stranky
		if (isset($_GET['pageno'])) {
					$pageno = $_GET['pageno'];
			}
		else {
			$pageno = 1;
		}
		$records_per_page = 1;
		$offset = ($pageno-1)*$records_per_page;
	
		//připojení do DB
		$conn=mysqli_connect("wa.toad.cz","savvavic","webove aplikace","savvavic");
		//pokud se nezadařilo
		if (mysqli_connect_errno()){
			echo "Failed to connect to DB: " . mysqli_connect_error();
			die();
		}
		//spočítat množství položek
		$total_pages_sql = "SELECT COUNT(*) FROM FAVORITES";
		//dotaz na DB
		$result = mysqli_query($conn,$total_pages_sql);
		//množství položek
		$total_rows = mysqli_fetch_array($result)[0];
		//množství stránek; ceil = zaokrouhlení do největšího čísla
		$total_pages = ceil($total_rows / $records_per_page);
		$email = $_SESSION['user'];
		//SQL příkaz pro paginaci/strankování
		$sql = "SELECT * FROM FAVORITES WHERE EMAIL = '$email' LIMIT $offset, $records_per_page";
		$res_data = mysqli_query($conn,$sql);
		$row = mysqli_fetch_array($res_data);
		$name = $row['NAME'];
		$sql2 = "SELECT * FROM RESOURCES WHERE NAME = '$name'";
		$res_data2 = mysqli_query($conn,$sql2);
		$num = mysqli_num_rows($res_data2);
		//pokud uzvatel ma oblibene polozky - vypis
		if ($num) {
			while ($row2 = mysqli_fetch_array($res_data2)) {
				echo "<h2>Category: <mark>".$row2['CATEGORY']."</mark> Name: <mark>".$row2['NAME']."</mark> Language: <mark>".$row2['LANGUAGE']."</mark> Year: <mark>".$row2['YEAR']."</mark><br><br> Link: <a id='read1' href='".$row2['LINK']."'>Read</a> <a id='read2' href='".$row2['LINK']."' download>Download</a><br><br><img src='".$row2['PIC']."' alt='no pic:('></h2><br><form method='post'><button id='rem' type='submit'><i class='fa fa-trash'></i> Remove from Favorites</button></form><br>";
				if ($_SERVER['REQUEST_METHOD'] == 'POST') {
					$email = $_SESSION['user'];
					$name = $row['NAME'];
					$sql = "DELETE FROM FAVORITES WHERE NAME = '$name'";
					$res = mysqli_query($conn, $sql);
					echo "<h2>Removed</h2><br>";
		?><script>
			window.onload = function() {
				history.replaceState("", "", "#");
			}
		</script>
		<?php
				}
			}
		}
		else {
			echo "<h2>No Items</h2><br>";
		}
		mysqli_close($conn);		 
		?>
		<ul class="pagination">
		<li><a href="?pageno=1">First</a></li>
		<li class="<?php if($pageno <= 1){ echo 'disabled'; } ?>">
			<a href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1); } ?>">Prev</a>
		</li>
		<li class="<?php if($pageno >= $total_pages){ echo 'disabled'; } ?>">
			<a href="<?php if($pageno >= $total_pages){ echo '#'; } else { echo "?pageno=".($pageno + 1); } ?>">Next</a>
		</li>
		<li><a href="?pageno=<?php echo $total_pages; ?>">Last</a></li>
		</ul>
	</body>
</html>
<?php } ?>