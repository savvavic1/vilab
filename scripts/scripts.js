function validate() {
    var user = document.loginform.username.value;
    var pass = document.loginform.password.value;
    if (user === "") {
        alert("Username can't be blank.");
        return false;
    }
    if (pass === "") {
        alert("Password can't be blank.");
        return false;
    }
    if (strlen(user) < 2) {
        alert("Username must be at least 3 characters long.");
        return false;
    }
}