function validate() {
    var email = document.reg.email.value;
    var password = document.reg.password.value;
    var pswrepeat= document.reg.pswrepeat.value;
    if (email === "") {
        alert("Email can't be blank.");
        return false;
    }
	else if (password==="") {
		alert("Password can't be blank");
		return false;
	}
    else if (password.length<6) {
        alert("Password must be at least 6 characters long.");
        return false;
    }
    else if (password!= pswrepeat) {
        alert("Passwords don't match!");
        return false;
    }
}